package Labyrinth.GUI;

import Labyrinth.*;
import Labyrinth.board.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.*;

/**
 * Grafická třída hracího pole.
 * Třída reprezentující hrací pole
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class Field extends javax.swing.JFrame {

    /**
	 * Konstruktor třídy Field.
	 * Konstruktor vytvoří novou hru a hrací plochu.
	 */
    public Field() {
        initComponents();
		
		if(Main.game == null){
			this.game = new Game();
		}
		else{
			this.game = Main.game;
			if (this.game.gameState == 1){
				JMust.setVisible(false);
			}
		}
		
		initBoard();
        setLocationRelativeTo(null);
		
		Jplayer.setText(this.game.actPlayer.name);
		JplayerScore.setText(Integer.toString(this.game.actPlayer.score) + "/" + Data.treasuresCnt/this.game.playersCnt);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Board = new javax.swing.JPanel();
        playerLable = new javax.swing.JLabel();
        Jplayer = new javax.swing.JLabel();
        playerScoreLabel = new javax.swing.JLabel();
        JplayerScore = new javax.swing.JLabel();
        cardPack = new JLabel () {

            @Override
            public void paintComponent (Graphics g) {
                // super.paintComponent (g);
                g.drawImage (card.getImage(), 0, 0, getWidth (), getHeight (), null);
            }
        };
        freeCard = new JLabel () {

            @Override
            public void paintComponent (Graphics g) {
                //super.paintComponent (g);
                g.drawImage (freeStoneImage.getImage(), 0, 0, getWidth (), getHeight (), null);
            }
        };
        jButton1 = new javax.swing.JButton();
        JMust = new javax.swing.JLabel();
        jBack = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Board.setBackground(new java.awt.Color(0, 0, 255));
        Board.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        Board.setToolTipText(null);
        Board.setMinimumSize(new java.awt.Dimension(100, 100));
        Board.setName(""); // NOI18N
        java.awt.GridBagLayout BoardLayout = new java.awt.GridBagLayout();
        BoardLayout.columnWidths = new int[] {Data.fieldSize};
        BoardLayout.rowHeights = new int[] {Data.fieldSize};
        Board.setLayout(BoardLayout);

        playerLable.setText("Actual player:");

        playerScoreLabel.setText("Score:");
        playerScoreLabel.setName(""); // NOI18N

        cardPack.setText("Balíček");
        cardPack.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        cardPack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardPackMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                cardPackMouseReleased(evt);
            }
        });

        freeCard.setText("freeCard");
        freeCard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                freeCardMousePressed(evt);
            }
        });

        jButton1.setText("Next Player");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        JMust.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        JMust.setForeground(new java.awt.Color(255, 0, 0));
        JMust.setText("        !!! INSERT STONE !!!");

        jBack.setText("jBack");
        jBack.setMaximumSize(new java.awt.Dimension(50, 50));
        jBack.setMinimumSize(new java.awt.Dimension(50, 50));
        jBack.setPreferredSize(new java.awt.Dimension(50, 50));
        jBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBackMouseClicked(evt);
            }
        });

        jMenu1.setText("New Game");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu1MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Save Game");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Board, javax.swing.GroupLayout.PREFERRED_SIZE, 725, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(playerScoreLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(playerLable)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Jplayer, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(JplayerScore)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cardPack, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(freeCard, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(15, 15, 15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JMust, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Jplayer)
                            .addComponent(playerLable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(playerScoreLabel)
                            .addComponent(JplayerScore))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBack, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(71, 71, 71)
                        .addComponent(freeCard, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JMust, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cardPack, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(Board, javax.swing.GroupLayout.DEFAULT_SIZE, 725, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MouseClicked
		int result = JOptionPane.showConfirmDialog(null, "Do you really want to create a new game? Any unsaved progress will be lost.", 
        "Quit?", JOptionPane.YES_NO_OPTION);
		if(result == JOptionPane.OK_OPTION){
			this.setVisible(false);
			new Menu().setVisible(true);
		}
    }//GEN-LAST:event_jMenu1MouseClicked

    private void freeCardMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_freeCardMousePressed
		this.game.board.free.turnRight();
		this.freeStoneImage = this.game.board.free.icon;
		this.freeCard.setIcon(this.freeStoneImage);
    }//GEN-LAST:event_freeCardMousePressed

    private void cardPackMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cardPackMousePressed
		this.card = new ImageIcon(this.cardFront.getImage());
		this.cardPack.setIcon(this.cardFront);
    }//GEN-LAST:event_cardPackMousePressed

    private void cardPackMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cardPackMouseReleased
		this.card = this.cardBack;
		this.cardPack.setIcon(this.cardBack);
    }//GEN-LAST:event_cardPackMouseReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
		if(this.game.gameState > 0){
			checkPlayerTreasure();
			nextPlayer();
		}
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
        // TODO add your handling code here:
		//this.game.SaveGame();
		//Main.saveGame(this.game);
		JFileChooser saveFile = new JFileChooser();
        
		if (saveFile.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			if (Main.saveGame(this.game, saveFile.getSelectedFile())){
				JOptionPane.showMessageDialog(null, "Game saved.", "Saving game",  JOptionPane.PLAIN_MESSAGE);
			}
			else JOptionPane.showMessageDialog(null, "Error occurred while saving!!", "Saving game",  JOptionPane.WARNING_MESSAGE);
		}
    }//GEN-LAST:event_jMenu2MouseClicked

    private void jBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBackMouseClicked
        if(this.game.gameState == 2){
			ImageIcon icon = this.game.board.get(this.game.actPlayer.getPosition()).getCard().icon;
			this.fields[this.game.actPlayer.getPosition()].setIcon(new ImageIcon(Main.getScaledImage(icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize)));
			this.game.actPlayer.revertPositions();
			this.game.gameState--;
			drawPlayers();

		}
		else if(this.game.gameState == 1){
			this.game.board.revertBoard();
			this.game.gameState--;
			for(int i = 0; i < Data.fieldSize; i++){
				redrawRow(i);				
			}
			drawPlayers();			
			JMust.setVisible(true);
			this.freeStoneImage = this.game.board.free.icon;
			this.freeCard.setIcon(this.freeStoneImage);

			this.game.gameState = 0;
		}
    }//GEN-LAST:event_jBackMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Field.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Field.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Field.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Field.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Field().setVisible(true);
            }
        });
		
    }
	
	/**
	 * inicializace hrací plochy
	 */
	private void initBoard(){
		int pos;
		ImageIcon field;
		GridBagConstraints c = new GridBagConstraints();
		for(int y = 0; y < Data.fieldSize; y++){
			for (int x = 0; x < Data.fieldSize; x++){
				pos = Data.fieldSize*y + x;
				field = new ImageIcon(getScaledImage(this.game.board.get(pos).getCard().icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize));
				this.fields[pos] = new JLabel();
				this.fields[pos].setIcon(field);
				final String name = x+"-"+y;
				this.fields[pos].setName(name);
				this.fields[pos].addMouseListener(new java.awt.event.MouseAdapter() {
					@Override
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						fieldClicked(evt, name);
					};
				});
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridx = x;
				c.gridy = y;
				this.Board.add(this.fields[pos], c);
			}
		}
		

		ImageIcon tmp = new ImageIcon(getClass().getResource("/Images/back.png"));

		tmp = new ImageIcon(getScaledImage(tmp.getImage(), 50, 50));
		this.jBack.setIcon(tmp);
		
		this.cardBack = new ImageIcon(getClass().getResource("/Images/cards/back2.png"));
		this.cardFront = new ImageIcon(getClass().getResource("/Images/cards/"+ this.game.actPlayer.card.tr.id +".png"));	
		this.card = this.cardBack;

		/*ImageIcon test = new ImageIcon("Images/treasures/ghost.png");
		test = new ImageIcon(Main.connectImages(this.cardBack.getImage(), test.getImage(), Main.LEFT_BOT_CORNER));*/
		
		this.card = cardBack;
		this.cardPack.setIcon(this.cardBack);

		this.freeStoneImage = this.game.board.free.icon;
		
		drawPlayers();
	}
	
	/**
	 * Funkce pro změnu velikosti obrázku
	 * @param srcImg zdrojový obrázek
	 * @param w očekávaná šířka obrázku
	 * @param h očekávaná výška obrázku
	 * @return změněný obrázek
	 */
	 private Image getScaledImage(Image srcImg, int w, int h){
		BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = resizedImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(srcImg, 0, 0, w, h, null);
		g2.dispose();
		return resizedImg;
	}
	
	/**
	 * Funkce vloží kámen nebo posune hráče
	 * @param evt
	 * @param name souřadnice [x-y]
	 */
	private void fieldClicked(java.awt.event.MouseEvent evt, String name) {                                    
		if(this.game.gameState == 0){
			insertStone(name);		
		}
		else if(this.game.gameState == 1){
			movePlayer(name);			
		}
		
		// po vlozeni kamene zmizi varovani
		if (this.game.gameState == 1){
			JMust.setVisible(false);
		}
    }
	
	/**
	 * Funkce vloží kámen, pokud je to možné
	 * @param name souřadnice [x-y]
	 */
	public void insertStone(String name){

		String[] a = name.split("-");
		int x = Integer.parseInt(a[0]);
		int y = Integer.parseInt(a[1]);
		
		boolean shifted = this.game.shift(this.game.board.get(y, x));
		
		if(shifted){
			if(y == 0 || y == Data.fieldSize-1){
				redrawColumn(x);
				this.freeStoneImage = this.game.board.free.icon;
				this.freeCard.setIcon(this.freeStoneImage);
			}
			else{
				redrawRow(y);
				this.freeStoneImage = this.game.board.free.icon;
				this.freeCard.setIcon(this.freeStoneImage);
			}
			drawPlayers();
			this.game.gameState++;
		}
	}
	
	/**
	 * Funkce pro překreslení sloupce
	 * @param x souřadnice daného sloupce
	 */
	public void redrawColumn(int x){
		boolean redraw = false;
		for(int y = 0; y < Data.fieldSize; y++){
			int pos = Data.fieldSize*y + x;		
			ImageIcon field = new ImageIcon(getScaledImage(this.game.board.get(pos).getCard().icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize));
			this.fields[pos].setIcon(field);
		}
	}
	
	/**
	 * Funkce pro překreslení řádku
	 * @param y souřadnice daného řádku
	 */
	public void redrawRow(int y){
		for(int x = 0; x < Data.fieldSize; x++){
			int pos = Data.fieldSize*y + x;
			ImageIcon field = new ImageIcon(getScaledImage(this.game.board.get(pos).getCard().icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize));
			this.fields[pos].setIcon(field);
		}
	}
	
	/**
	 * Funkce, která posune hráče
	 * @param name jméno hráče
	 */
	public void movePlayer(String name){
		String[] a = name.split("-");
		int x = Integer.parseInt(a[0]);
		int y = Integer.parseInt(a[1]);
		
		this.game.initCords(this.game.actPlayer.getX(), this.game.actPlayer.getY());
		boolean canMove = this.game.path(this.game.actPlayer.getX(), this.game.actPlayer.getY(), x, y);
		if(canMove){
			int newPos = Data.fieldSize*y + x;
			int oldPos = this.game.actPlayer.getPosition();
			this.game.actPlayer.setPosition(x, y);

			this.game.board.get(oldPos).getCard().iconAct = this.game.board.get(oldPos).getCard().icon;
			ImageIcon icon = this.game.board.get(oldPos).getCard().iconAct;
			this.fields[oldPos].setIcon(new ImageIcon(Main.getScaledImage(icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize)));

			drawPlayers();
			this.game.gameState++;
		}
    }  
	
	/**
	 * Funkce pro přepnutí na dalšího hráče
	 */
	private void nextPlayer(){
		if (this.game.actPlayer.score == Data.treasuresCnt/this.game.playersCnt){
			JOptionPane.showMessageDialog(null, "Vyhrál jsi!!!", "Výhra",  JOptionPane.PLAIN_MESSAGE);
			this.setVisible(false);
			new Menu().setVisible(true);
		}
			
		
			this.game.nextPlayer();			
			Jplayer.setText(this.game.actPlayer.name);
			JplayerScore.setText(Integer.toString(this.game.actPlayer.score) + "/" + Data.treasuresCnt/this.game.playersCnt);
			JMust.setVisible(true);
			this.cardFront = new ImageIcon(getClass().getResource("/Images/cards/"+ this.game.actPlayer.card.tr.id +".png"));

			this.game.gameState = 0;

			//this.game.TreasureTest();
	}
	
	/**
	 * Funkce se stará o správné volání funkce checkPlayerTreasure
	 */
	private void checkPlayerTreasure(){
		this.game.checkPlayerTreasure();
	}
	
	/**
	 * Funkce pro vykreslení hráčů
	 */
	public void drawPlayers(){
		int pos = this.game.player1.getPosition();
		ImageIcon icon = this.game.board.get(pos).getCard().iconAct;
		icon = this.game.board.get(pos).getCard().iconAct = new ImageIcon(Main.connectImages(icon.getImage(), this.game.player1.icon.getImage(), Main.LEFT_TOP_CORNER));
		this.fields[pos].setIcon(new ImageIcon(Main.getScaledImage(icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize)));
		
		pos = this.game.player2.getPosition();
		icon = this.game.board.get(pos).getCard().iconAct;
		icon = this.game.board.get(pos).getCard().iconAct = new ImageIcon(Main.connectImages(icon.getImage(), this.game.player2.icon.getImage(), Main.RIGHT_TOP_CORNER));
		this.fields[pos].setIcon(new ImageIcon(Main.getScaledImage(icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize)));
		
		if(this.game.playersCnt > 2){
			pos = this.game.player3.getPosition();
			icon = this.game.board.get(pos).getCard().iconAct;
			icon = this.game.board.get(pos).getCard().iconAct = new ImageIcon(Main.connectImages(icon.getImage(), this.game.player3.icon.getImage(), Main.LEFT_BOT_CORNER));
		this.fields[pos].setIcon(new ImageIcon(Main.getScaledImage(icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize)));
			if(this.game.playersCnt == 4){
				pos = this.game.player4.getPosition();
				icon = this.game.board.get(pos).getCard().iconAct;
				icon = this.game.board.get(pos).getCard().iconAct = new ImageIcon(Main.connectImages(icon.getImage(), this.game.player4.icon.getImage(), Main.RIGHT_BOT_CORNER));
		this.fields[pos].setIcon(new ImageIcon(Main.getScaledImage(icon.getImage(), this.Board.getWidth()/Data.fieldSize, this.Board.getHeight()/Data.fieldSize)));
			}
		}
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Board;
    private javax.swing.JLabel JMust;
    private javax.swing.JLabel Jplayer;
    private javax.swing.JLabel JplayerScore;
    private javax.swing.JLabel cardPack;
    private javax.swing.JLabel freeCard;
    private javax.swing.JLabel jBack;
    private javax.swing.JButton jButton1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel playerLable;
    private javax.swing.JLabel playerScoreLabel;
    // End of variables declaration//GEN-END:variables
	private Game game;
	private JLabel[] fields = new JLabel[Data.fieldSize*Data.fieldSize];
	private ImageIcon cardBack;
	private ImageIcon cardFront;
	private ImageIcon card;
	private ImageIcon freeStoneImage;
}
