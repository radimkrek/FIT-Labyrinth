package Labyrinth;

/**
 * Třída dat.
 * Třída uchovávajíci si informace potřebné po celý běh programu
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class Data {
		
	/** 
	 * Počet kamenů na hrací desce v jednom řádku (sloupci)
	 */
	public static int fieldSize = 7;
	
	/**
	 * Počet vygenerovaných pokladů
	 */
	public static int treasuresCnt;
	
	/**
	 * Jméno prvního hráče
	 */
	public static String player1;
	
	/**
	 * Jméno druhého hráče
	 */
	public static String player2;
	
	/**
	 * Jméno třetího hráče
	 */
	public static String player3;
	
	/**
	 * Jméno čtvrtého hráče
	 */
	public static String player4;
}
