package Labyrinth;

import Labyrinth.treasure.*;
import javax.swing.ImageIcon;

/**
 * Třída hráče.
 * Třída reprezentující jednoho hráče ve hře
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class Player implements java.io.Serializable{
	public String name;
	public CardPack pack;
	public TreasureCard card = null;
	private int[] position = new int[2];
	private int[] oldPosition = new int[2];
	public ImageIcon icon;
	public int score;
	
	/**
	 * Vytvoří nového hráče a nastaví mu jeho ikonku
	 * @param name jméno hráče
	 * @param id Id ikonky pro hráče
	 */
	public Player(String name, int id){
		this.name = name;
		this.icon = new ImageIcon(getClass().getResource("/Images/player/"+id+".png"));
		this.score = 0;
	}
	
	/**
	 * Vytáhne z alíčku kartu a uloží ji jako aktuální
	 */
	public void popCard(){
		if(this.pack.size() > 0){
			this.card = this.pack.popCard();
		}
		else{
			this.card = null;
		}
	}
	/**
	 * Posune hráče na pozici
	 * @param x X-ová souřadnice
	 * @param y Y-ová souřadnice
	 */
	public void setPosition(int x, int y){
		this.oldPosition[0] = this.position[0];
		this.oldPosition[1] = this.position[1];
		this.position[0] = x;
		this.position[1] = y;
	}
	/**
	 * @return vrátí pozici hráče
	 */
	public int getPosition(){
		return Data.fieldSize*this.getY()+this.getX();
	}
	/**
	 * @return vrátí X-ovou souřadnici hráče
	 */
	public int getX(){
		return this.position[0];
	}
	/**
	 * @return vrátí Y-ovou souřadnici hráče
	 */
	public int getY(){
		return this.position[1];
	}
	/**
	 * Nastaví X-ovou souřadnici hráče
	 * @param x souřadnice
	 */
	public void setX(int x){
		this.oldPosition[0] = this.position[0];
		this.position[0] = x;
	}
	/**
	 * Nastaví Y-ovou souřadnici hráče
	 * @param y souřadnice
	 */
	public void setY(int y){
		this.oldPosition[1] = this.position[1];
		this.position[1] = y;
	}
	
	/**
	 * Nastaví starou pozici jako aktuální
	 */
	public void revertPositions(){
		int[] temp = new int[2];
		temp[0] = this.position[0];
		temp[1] = this.position[1];
		this.position[0] = this.oldPosition[0];
		this.position[1] = this.oldPosition[1];
		this.oldPosition[0] = temp[0];
		this.oldPosition[1] = temp[1];
	}
}
