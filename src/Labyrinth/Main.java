package Labyrinth;

import Labyrinth.board.*;
import Labyrinth.treasure.*;
import Labyrinth.GUI.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Hlavní třída.
 * Hlavní třída, která se stará o spuštění programu.
 * Obsahuje také statické funkce a konstanty, které jsou využívány dále v programu.
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class Main {
	/**
	 * Konstanta umístění do levého horního rohu.
	 */
	public static final int LEFT_TOP_CORNER = 0;
	/**
	 * Konstanta umístění do pravého horního rohu.
	 */
	public static final int RIGHT_TOP_CORNER = 1;
	/**
	 * Konstanta umístění do pravého dolního rohu.
	 */
	public static final int RIGHT_BOT_CORNER = 2;
	/**
	 * Konstanta umístění do levého dolního rohu.
	 */
	public static final int LEFT_BOT_CORNER = 3;
	/**
	 * Konstanta umístění do horního středu.
	 */
	public static final int TOP_CENTER = 4;
	/**
	 * Konstanta umístění do pravého středu.
	 */
	public static final int RIGHT_CENTER = 5;
	/**
	 * Konstanta umístění do spodního středu.
	 */
	public static final int BOT_CENTER = 6;
	/**
	 * Konstanta umístění do levého středu.
	 */
	public static final int LEFT_CENTER = 7;
	/**
	 * Konstanta umístění do středu.
	 */
	public static final int CENTER = 8;
	
	/**
	 * Proměnná, pro uložení rozrhrané hry načtené ze souboru
	 * @see Labyrinth.Game
	 */
	public static Game game = null;
    
	// Spustí program
    Menu gui = new Labyrinth.GUI.Menu();
	
	/**
	 * Přeformátování obrázku.
	 * Funkce naškáluje vstupní obrázek na požadovanou velikost
	 * @param srcImg vstupní obrázek
	 * @param w požadovaná šířka
	 * @param h požadovaná výška
	 * @return Naformátovaný obrázek
	 */
	public static Image getScaledImage(Image srcImg, int w, int h){
		BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = resizedImg.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(srcImg, 0, 0, w, h, null);
		g2.dispose();
		return resizedImg;
	}
	
	/**
	 * Spojení obrázků.
	 * Funkce starající se o spojení dvou obrázků do jednoho
	 * @param bg Obrázek, který bude napozadí
	 * @param fg Obrázek, který bude na popředí
	 * @param position Určuje jak má byt pední obrázek umístěn vzhledem k pozadí. Využívají se zde konstanty z třídy Main
	 * @return obrázek, vzniklý spojením vstupních obrázků
	 */
	public static Image imageOnImage(Image bg, Image fg, int position){
		// knevertuju i druhý obrázek
		final BufferedImage BBG = new BufferedImage(bg.getWidth(null),bg.getHeight(null),BufferedImage.TYPE_INT_ARGB);
		Graphics g2 = BBG.getGraphics();
        g2.drawImage(bg,0,0,BBG.getWidth(),BBG.getHeight(),null);
        g2.dispose();
		
		// prekonvertuju si prvni obrazek na spravny typ pro vypocty
		final BufferedImage BFG = new BufferedImage(fg.getWidth(null),fg.getHeight(null),BufferedImage.TYPE_INT_ARGB);
		Graphics g1 = BFG.getGraphics();
        g1.drawImage(fg,0,0,BFG.getWidth(),BFG.getHeight(),null);
        g1.dispose();
		
		int[] pos = getPos(BBG.getWidth(), BBG.getHeight(), BFG.getWidth(), BFG.getHeight(), position);
		
		// Spojím obrázky dohromady
		Graphics g = BBG.getGraphics();
		g.drawImage(BFG, pos[0], pos[1], null);
		g.dispose();
		
		return BBG;
	}
	// vstupní funkce pro volání spojení obrázků
	public static Image connectImages(Image bg, Image fg, int position){
		return imageOnImage(bg, fg, position);
	}
	// s resizem pozadí
	public static Image connectImages(Image bg, int w, int h, Image fg, int position){
		return imageOnImage(getScaledImage(bg, w, h), fg, position);
	}
	// s resizem popredi
	public static Image connectImages(Image bg, Image fg, int w, int h, int position){
		return imageOnImage(bg, getScaledImage(fg, w, h), position);
	}
	// resiz obou
	public static Image connectImages(Image bg, int w1, int h1, Image fg, int w2, int h2, int position){
		return imageOnImage(getScaledImage(bg, w1, h1), getScaledImage(fg, w2, h2), position);
	}
	
	private static int[] getPos(int w1, int h1, int w2, int h2, int position){
		int[] pos = new int[2];
		if(position == Main.LEFT_TOP_CORNER){
			pos[0] = 10;
			pos[1] = 10;
		}
		else if(position == Main.RIGHT_TOP_CORNER){
			pos[0] = w1-200;
			pos[1] = 10;
		}
		else if(position == Main.RIGHT_BOT_CORNER){
			pos[0] = w1-200;
			pos[1] = h1 - h2 -10;
		}
		else if(position == Main.LEFT_BOT_CORNER){
			pos[0] = 10;
			pos[1] = h1 - h2 - 10;
		}
		else if(position == Main.TOP_CENTER){
			pos[0] = w1/2 - w2/2;
			pos[1] = 10;
		}
		else if(position == Main.RIGHT_CENTER){
			pos[0] = w1-200;
			pos[1] = h1/2 - h2/2;
		}
		else if(position == Main.BOT_CENTER){
			pos[0] = w1/2 - w2/2;
			pos[1] = h1 - h2 - 10;
		}
		else if(position == Main.LEFT_CENTER){
			pos[0] = 10;
			pos[1] = h1/2 - h2/2;
		}
		else if(position == Main.CENTER){
			pos[0] = w1/2 - w2/2;
			pos[1] = h1/2 - h2/2;
		}
		return pos;
	}
	
	public static boolean inArray(final int[] array, final int key){		
		for(int i = 0; i < array.length; i++){
			if(array[i] == key){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Funkce uložení hry.
	 * @param game Objet třídy Labyrinth.Game reprezentující celou hru
	 * @param file Požadovaný soubor, kam se má hra uložit
	 * @return vrátí zda se povedlo uložit nebo ne
	 */
	public static boolean saveGame(Game game, File file){
		try{
			FileOutputStream saveFile=new FileOutputStream(file);
			ObjectOutputStream save = new ObjectOutputStream(saveFile);
			save.writeObject(Data.fieldSize);
			save.writeObject(Data.player1);
			save.writeObject(Data.player2);
			save.writeObject(Data.player3);
			save.writeObject(Data.player4);
			save.writeObject(Data.treasuresCnt);
			save.writeObject(game);
			saveFile.close();
		}
		catch(Exception exc){
			System.out.println(exc);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Funkce načtení hry
	 * @param file Soubor, ze kterého má být hra načtena
	 * @return vrací zda se povedlo načíst nebo ne
	 */
	public static boolean loadGame(File file){
		try{
			FileInputStream saveFile = new FileInputStream(file);
			ObjectInputStream save = new ObjectInputStream(saveFile);
			Data.fieldSize = (int) save.readObject();
			Data.player1 = (String) save.readObject();
			Data.player2 = (String) save.readObject();
			Data.player3 = (String) save.readObject();
			Data.player4 = (String) save.readObject();
			Data.treasuresCnt = (int) save.readObject();
			Main.game = (Game) save.readObject();
		}
		catch(Exception exc){
			System.out.println(exc);
			return false;
		}
		return true;
	}
}
