package Labyrinth.treasure;

import java.util.Objects;

/**
 * Třída reprezentující kartu v balíčku
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class TreasureCard implements java.io.Serializable{
	
	public Treasure tr;
	
	/**
	 * Konstruktor, který nastaví typ karty.
	 * @param tr typ karty
	 * @see Labyrinth.treasure.Treasure
	 */
	public TreasureCard(Treasure tr){
		this.tr = tr;
	}

	@Override
	public int hashCode() {
		return tr.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final TreasureCard other = (TreasureCard) obj;
		if (!Objects.equals(this.tr, other.tr)) {
			return false;
		}
		return true;
	}


	
	
}