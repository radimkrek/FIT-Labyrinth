package Labyrinth.treasure;

import javax.swing.ImageIcon;

/**
 * Třída reprezentující typ karty a funkce pro vygenerování všech karet
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class Treasure implements java.io.Serializable{
	
	/**
	 * Maximální počet karet, které může vygenerovat
	 */
	public static int maxValue;
	/**
	 * Pole všech vygenerovaných typů
	 */
	private static Treasure[] objects;
	/**
	 * Obrázek, reprezentující dyný typ karty
	 */
	public ImageIcon icon = null;
	
	public int id;
	
	public Treasure(int code, boolean generateIcon){
		this.id = code;
		if(generateIcon){
			this.icon = new ImageIcon(getClass().getResource("/Images/treasures/"+this.id+".png"));
		}
	}
	
	public static void createSet(){
		Treasure.objects = new Treasure[Treasure.maxValue];
		for (int j = 0; j < Treasure.maxValue; j++) {
			Treasure.objects[j] = new Treasure(j, false);
		}
	}
	
	public static Treasure getTreasure(int code){
		try{
			return Treasure.objects[code];
		}
		catch(java.lang.Exception e){
			return null;
		}
	}
}