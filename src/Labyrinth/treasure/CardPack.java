package Labyrinth.treasure;

/**
 * Třída reprezentuje celý balíček karet
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class CardPack implements java.io.Serializable{

	/**
	 * maximální velikost balíčku
	 */
	private int maxSize;
	/**
	 * aktuální počet karet v balíčku
	 */
	private int size;
	/**
	 * pole všech karet
	 */
	public TreasureCard[] cards;

	/**
	 * Konstruktor, který inicializuje balíček a nechá jej vygenerovat
	 * @param maxSize maximální velikost balíčku
	 * @param initSize inicializační velikost
	 */
	public CardPack(int maxSize, int initSize) {
		this.maxSize = maxSize;
		this.size = initSize;
		this.cards = new TreasureCard[this.maxSize];
		Treasure.maxValue = maxSize;
		Treasure.createSet();
		int j = 0;
		for (int i = this.size - 1; i >= 0; i--) {
			this.cards[j++] = new TreasureCard(Treasure.getTreasure(i));
		}
	}
	
	/**
	 * Konstruktor, který přebere již vygenerované karty a umístí je do balíčku
	 * @param cards vygenerované karty
	 * @param size počet předávaných karet
	 */
	public CardPack(TreasureCard[] cards, int size) {
		this.cards = cards;
		this.size = size;
	}
	
	/**
	 * @return vrací velikost balíčku
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Vyhodí kartu z balíku
	 * @return vrátí vytaženou kartu
	 */
	public TreasureCard popCard() {
		this.size--;
		return this.cards[this.size];
	}

	/**
	 * Funkce zamíchá aktuální balíček
	 */
	public void shuffle() {
		java.util.Random randomizer = new java.util.Random();
		TreasureCard temp;
		for (int i = 0; i < this.size; i++) {
			int rand = randomizer.nextInt(this.size - 1);
			temp = this.cards[rand];
			this.cards[rand] = this.cards[i];
			this.cards[i] = temp;
		}
	}
}
