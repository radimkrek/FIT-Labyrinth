package Labyrinth;

import Labyrinth.board.*;
import Labyrinth.treasure.*;
import javax.swing.JLabel;

/**
 * Hlavní třída hry.
 * Třída reprezentující celou hru.
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class Game implements java.io.Serializable{
	/**
	* Představuje stav hry.
	* 0 - nutnost vlozit kamen
	* 1 - nutnost posunout figurkou
	*/
	public int gameState = 0;
	
	/**
	 * Proměnná reprezentující hráče
	 * @see Labyrinth.Player
	 */
	public Player player1 = new Player(Data.player1, 0);
	
	/**
	 * Proměnná reprezentující hráče
	 * @see Labyrinth.Player
	 */
	public Player player2 = new Player(Data.player2, 1);
	
	/**
	 * Proměnná reprezentující hráče
	 * @see Labyrinth.Player
	 */
	public Player player3 = null;
	
	/**
	 * Proměnná reprezentující hráče
	 * @see Labyrinth.Player
	 */
	public Player player4 = null;

	private TreasureCard[] pack1, pack2, pack3, pack4;
	private CardPack pack;
	
	
	/**
	 * Poměnná reprezentující aktuálního hráče
	 * @see Labyrinth.Player
	 */
	public Player actPlayer;
	private Player [] players = new Player [4];
	
	
	/**
	 * Poměnná reprezentující počet hráčů
	 */
	public int playersCnt = 2;
	private int packCnt = Data.treasuresCnt;
	
	private int[][] cordArray;
	
	
	/**
	 * Poměnná reprezentující hrací plochu
	 * @see Labyrinth.board.MazeBoard
	 */
	public MazeBoard board;
	private JLabel[] fields = new JLabel[Data.fieldSize * Data.fieldSize];
	private int[] cordCounter;

	
	/**
	 * Konstruktor třídy Game.
	 * Konstruktor vytvoří hráče, umístí je na plochu, kterou konstruktor také nechal vygenerovat
	 */
	public Game() {
		this.player1.setPosition(0, 0);
		this.player2.setPosition(Data.fieldSize-1, 0);
		// pokud máme zadaného 3 a 4 hráče tak je vytvoříme.
		if (Data.player3.length() > 0) {
			this.player3 = new Player(Data.player3, 2);
			this.player3.setPosition(0, Data.fieldSize-1);
			playersCnt = 3;
		}
		
		if (Data.player4.length() > 0) {
			this.player4 = new Player(Data.player4, 3);
			this.player4.setPosition(Data.fieldSize-1, Data.fieldSize-1);
			playersCnt = 4;
		}

		actPlayer = player1;
				
		players[0] = player1; 
		players[1] = player2; 
		players[2] = player3; 
		players[3] = player4; 
		
		this.board = MazeBoard.createMazeBoard(Data.fieldSize);
		this.board.newGame();

		createCardPacks();
	}
	
	
	/**
	 * Funkce vytvoření balíčků.
	 * Funkce vytvoří balíček o požadovaném počtu karet, zamíchá jej a poté rozá všem hrajícím hráčům.
	 */
	private void createCardPacks(){
		// vytvoreni balicku		
		pack = new CardPack(packCnt, packCnt);
		pack.shuffle();
		
		if (Data.player4.length() > 0) {
			pack1 = new TreasureCard[packCnt/4];
			pack2 = new TreasureCard[packCnt/4];
			pack3 = new TreasureCard[packCnt/4];
			pack4 = new TreasureCard[packCnt/4];
			System.arraycopy( pack.cards, 0, pack1, 0, packCnt/4);
			System.arraycopy( pack.cards, packCnt/4, pack2, 0, packCnt/4);
			System.arraycopy( pack.cards, packCnt/2, pack3, 0, packCnt/4);
			System.arraycopy( pack.cards, (packCnt/4)*3, pack4, 0, packCnt/4);
						
			this.player1.pack = new CardPack(pack1,packCnt/4);
			this.player2.pack = new CardPack(pack2,packCnt/4);
			this.player3.pack = new CardPack(pack3,packCnt/4);
			this.player4.pack = new CardPack(pack4,packCnt/4);
			
			this.player1.popCard();
			this.player2.popCard();
			this.player3.popCard();
			this.player4.popCard();
		} 
		else if (Data.player3.length() > 0) {	
			pack1 = new TreasureCard[packCnt/3];
			pack2 = new TreasureCard[packCnt/3];
			pack3 = new TreasureCard[packCnt/3];
			System.arraycopy( pack.cards, 0, pack1, 0, packCnt/3);
			System.arraycopy( pack.cards, packCnt/3, pack2, 0, packCnt/3);
			System.arraycopy( pack.cards, (packCnt/3)*2, pack3, 0, packCnt/3);
			
			this.player1.pack = new CardPack(pack1,packCnt/3);
			this.player2.pack = new CardPack(pack2,packCnt/3);
			this.player3.pack = new CardPack(pack3,packCnt/3);
			
			this.player1.popCard();
			this.player2.popCard();
			this.player3.popCard();
		} 
		else {
			pack1 = new TreasureCard[12];
			pack2 = new TreasureCard[12];
			System.arraycopy( pack.cards, 0, pack1, 0, packCnt/2);
			System.arraycopy( pack.cards, packCnt/2, pack2, 0, packCnt/2);
			
			this.player1.pack = new CardPack(pack1,packCnt/2);
			this.player2.pack = new CardPack(pack2,packCnt/2);
			
			this.player1.popCard();
			this.player2.popCard();
		}
	}
	
	
	/**
	 * Funkce na změnu aktuálního hráče.
	 */
	public void nextPlayer(){		
		if(this.playersCnt == 2){
			if(this.actPlayer == this.player1){
				this.actPlayer = this.player2;
			}
			else if(this.actPlayer == this.player2){
				this.actPlayer = this.player1;
			}
		}
		
		if(this.playersCnt == 3){
			if(this.actPlayer == this.player1){
				this.actPlayer = this.player2;
			}
			else if(this.actPlayer == this.player2){
				this.actPlayer = this.player3;
			}
			else if(this.actPlayer == this.player3){
				this.actPlayer = this.player1;
			}
		}
		
		if(this.playersCnt == 4){
			if(this.actPlayer == this.player1){
				this.actPlayer = this.player2;
			}
			else if(this.actPlayer == this.player2){
				this.actPlayer = this.player3;
			}
			else if(this.actPlayer == this.player3){
				this.actPlayer = this.player4;
			}
			else if(this.actPlayer == this.player4){
				this.actPlayer = this.player1;
			}
		}
		
	}
	
	/**
	 * Funkce pro detekci pokladu.
	 * Funkce zjišťuje, jestli se na políčku aktuálního hráče nachází poklad, který hledá
	 */
	public void checkPlayerTreasure(){
		TreasureCard actCard = actPlayer.card;
		int actX = actPlayer.getX();
		int actY = actPlayer.getY();
		MazeField actField = board.get(actY, actX);		
		
		if (actField.getCard().treasure != null){
			if (actCard.tr.id == actField.getCard().treasure.id){		
				actPlayer.popCard();
				actPlayer.score += 1;			
			}
		}

		
	}
	
	
	/**
	 * Funkce posunu pole.
	 * Funkce zahájí posun řádku nebo sloupce a pokud musí posune i hráče na požadované pole. Posun neprovádí tato funkce ale volá {@link} Labyrinth.board.MazeBoard
	 * @see Labyrinth.board.MazeBoard
	 * @param mf proměnná reprezentující pole na které hráč kliknul
	 * @return vrací zda hrací plochou pohnul nebo ne
	 * @see Labyrinth.board.MazeField
	 */
	public boolean shift(MazeField mf){
		boolean shifted = this.board.shift(mf);
		boolean p1, p2, p3, p4;
		p1 = p2 = p3 = p4 = true;
		if(shifted){		
			if(mf.row() == 1 && mf.col() %2 == 0){
				for(int y = Data.fieldSize; y >= 0; y--){
					if(p1 && this.player1.getY() == y && this.player1.getX() == mf.col()-1){
						this.player1.setPosition(mf.col()-1, (this.player1.getY()+1)%Data.fieldSize);
						p1 = false;
					}
					if(p2 && this.player2.getY() == y && this.player2.getX() == mf.col()-1){
						this.player2.setPosition(mf.col()-1, (this.player2.getY()+1)%Data.fieldSize);
						p2 = false;
					}
					if(p3 && this.playersCnt > 2 && this.player3.getY() == y && this.player3.getX() == mf.col()-1){
						this.player3.setPosition(mf.col()-1, (this.player3.getY()+1)%Data.fieldSize);
						p3 = false;
					}
					if(p4 && this.playersCnt > 3 && this.player4.getY() == y && this.player4.getX() == mf.col()-1){
						this.player4.setPosition(mf.col()-1, (this.player4.getY()+1)%Data.fieldSize);
						p4 = false;
					}
				}
			}
			else if(mf.row() ==  Data.fieldSize && mf.col() %2 == 0){
				for(int y = 0; y < Data.fieldSize; y++){
					if(p1 &&this.player1.getY() == y && this.player1.getX() == mf.col()-1){
						if(this.player1.getY() == 0){
							this.player1.setY(Data.fieldSize);
						}
						this.player1.setPosition(mf.col()-1, (this.player1.getY()-1)%Data.fieldSize);
						p1 = false;
					}
					if(p2 && this.player2.getY() == y && this.player2.getX() == mf.col()-1){
						if(this.player2.getY() == 0){
							this.player2.setY(Data.fieldSize);
						}
						this.player2.setPosition(mf.col()-1, (this.player2.getY()-1)%Data.fieldSize);
						p2 = false;
					}
					if(p3 && this.playersCnt > 2 && this.player3.getY() == y && this.player3.getX() == mf.col()-1){
						if(this.player3.getY() == 0){
							this.player3.setY(Data.fieldSize);
						}
						this.player3.setPosition(mf.col()-1, (this.player3.getY()-1)%Data.fieldSize);
						p3 = false;
					}
					if(p4 && this.playersCnt > 3 && this.player4.getY() == y && this.player4.getX() == mf.col()-1){
						if(this.player4.getY() == 0){
							this.player4.setY(Data.fieldSize);
						}
						this.player4.setPosition(mf.col()-1, (this.player4.getY()-1)%Data.fieldSize);
						p4 = false;
					}
				}
			}
			else if(mf.col() == 1 && mf.row() %2 == 0){
				for(int x = Data.fieldSize; x >= 0; x--){
					if(p1 && this.player1.getX() == x && this.player1.getY() == mf.row()-1){
						this.player1.setPosition((this.player1.getX()+1)%Data.fieldSize, mf.row()-1);
						p1 = false;
					}
					if(p2 && this.player2.getX() == x && this.player2.getY() == mf.row()-1){
						this.player2.setPosition((this.player2.getX()+1)%Data.fieldSize, mf.row()-1);
						p2 = false;
					}
					if(p3 && this.playersCnt > 2 && this.player3.getX() == x && this.player3.getY() == mf.row()-1){
						this.player3.setPosition((this.player3.getX()+1)%Data.fieldSize, mf.row()-1);
						p3 = false;
					}
					if(p4 && this.playersCnt > 3 && this.player4.getX() == x && this.player4.getY() == mf.row()-1){
						this.player4.setPosition((this.player4.getX()+1)%Data.fieldSize, mf.row()-1);
						p4 = false;
					}
				}
			}
			else if(mf.col() ==  Data.fieldSize && mf.row() %2 == 0){
				for(int x = 0; x < Data.fieldSize; x++){
					if(p1 && this.player1.getX() == x && this.player1.getY() == mf.row()-1){
						if(this.player1.getX() == 0){
							this.player1.setX(Data.fieldSize);
						}
						this.player1.setPosition((this.player1.getX()-1)%Data.fieldSize, mf.row()-1);
						p1 = false;
					}
					if(p2 && this.player2.getX() == x && this.player2.getY() == mf.row()-1){
						if(this.player2.getX() == 0){
							this.player2.setX(Data.fieldSize);
						}
						this.player2.setPosition((this.player2.getX()-1)%Data.fieldSize, mf.row()-1);
						p2 = false;
					}
					if(p3 && this.playersCnt > 2 && this.player3.getX() == x && this.player3.getY() == mf.row()-1){
						if(this.player3.getX() == 0){
							this.player3.setX(Data.fieldSize);
						}
						this.player3.setPosition((this.player3.getX()-1)%Data.fieldSize, mf.row()-1);
						p3 = false;
					}
					if(p4 && this.playersCnt > 3 && this.player4.getX() == x && this.player4.getY() == mf.row()-1){
						if(this.player4.getX() == 0){
							this.player4.setX(Data.fieldSize);
						}
						this.player4.setPosition((this.player4.getX()-1)%Data.fieldSize, mf.row()-1);
						p4 = false;
					}
				}
			}		
		}
		
		return shifted;
	}
	
	
	/**
	 * Inicializace pole pro ukládání
	 * inicializace pole pro ukládání projítých polí
	 * @param x X-ová souřadnice
	 * @param y Y-ová souřadnice
	 */
	public void initCords(int x, int y){
		this.cordCounter = new int[Data.fieldSize];
		this.cordArray = new int[Data.fieldSize][Data.fieldSize];
		for(int i = 0; i < Data.fieldSize; i++){
			for(int j = 0; j < Data.fieldSize; j++){
				this.cordArray[i][j] = -1;
			}
		}
		//this.cordArray[y][this.cordCounter[y]++] = x;
	}
	
	/**
	 * Kontrola unikátnosti pole
	 * Funkce je volána při hledání cesty pro hráče a kontroluje, zda na tomto políčku hráč byl a tudíž zde neni třeba znovu kontrolovat cestu.
	 * Pokud nalezne políčko kde hráč nebyl přidá jej do pole;
	 * @param x X-ová souřadnice
	 * @param y Y-ová souřadnice
	 * @return Vrací boolovskou hodnotu zda má hráč na pole vstoupit nebo ne
	 */
	public boolean checkCord(int x, int y){
		int val = this.cordArray[y][x];
		if(val == -1){
			this.cordArray[y][x]=1;
			return true;
		}
		else if(val == 1){
			return false;			
		}
		return false;
	}
	
	/**
	 * Kontrola cesty
	 * Funkce kontroluje zda hráč může jít na zvolené políčko. Funkce je volána rekurzivně a posouvá hráče postupně na pole kam může a kontroluje zda to neni požadované pole.
	 * @param x1 X-ová souřadnice na které hráč aktuálně stojí
	 * @param y1 Y-ová souřadnice na které hráč aktuálně stojí
	 * @param x2 X-ová souřadnice na kterou se hráč snaží dojít
	 * @param y2 Y-ová souřadnice na kterou se hráč snaží dojít 
	 * @return Vrací boolovskou hodnotu zda hráč došel nebo ne
	 */
	public boolean path(int x1, int y1, int x2, int y2){
		MazeField mf = this.board.get(y1, x1);
		MazeField mf2;
		//System.out.printf("%d,%d - L: %s, U: %s, R: %s, D: %s\n", x1, y1, mf.getCard().canGo(MazeCard.CANGO.LEFT), mf.getCard().canGo(MazeCard.CANGO.UP), mf.getCard().canGo(MazeCard.CANGO.RIGHT), mf.getCard().canGo(MazeCard.CANGO.DOWN));
		if(x1==x2 && y1==y2){
			return true;
		}
		if(mf.getCard().canGo(MazeCard.CANGO.UP) && y1 > 0){
			mf2 = this.board.get(y1-1, x1);
			if(mf2.getCard().canGo(MazeCard.CANGO.DOWN) && checkCord(x1, y1-1)){
				if(path(x1, y1-1, x2, y2)){
					return true;
				}
			}
		}
		if(mf.getCard().canGo(MazeCard.CANGO.RIGHT) && x1 < Data.fieldSize-1){
			mf2 = this.board.get(y1, x1+1);
			if(mf2.getCard().canGo(MazeCard.CANGO.LEFT) && checkCord(x1+1, y1)){
				if(path(x1+1,y1,x2,y2)){
					return true;
				}
			}
		}
		if(mf.getCard().canGo(MazeCard.CANGO.DOWN) && y1 < Data.fieldSize-1){
			mf2 = this.board.get(y1+1,x1);
			if(mf2.getCard().canGo(MazeCard.CANGO.UP) && checkCord(x1, y1+1)){
				if(path(x1,y1+1,x2,y2)){
					return true;
				}
			}
		}
		if(mf.getCard().canGo(MazeCard.CANGO.LEFT) && x1 > 0){
			mf2 = this.board.get(y1, x1-1);
			if(mf2.getCard().canGo(MazeCard.CANGO.RIGHT) && checkCord(x1-1, y1)){
					if(path(x1-1,y1,x2,y2)){
					return true;
				}
			}
		}
		return false;
	}

	public void TreasureTest(){
		MazeField actField;
		
		for(int x = 0; x < Data.fieldSize; x++){
			for (int y = 0; y < Data.fieldSize; y++){
				actField = board.get(x, y);
				
				if (actField.getCard().treasure != null){
					System.out.printf("%d.", actField.getCard().treasure.id);
				}
				else System.out.printf("X.");
			}
			
			System.out.printf("\n");
		}
	}
}
