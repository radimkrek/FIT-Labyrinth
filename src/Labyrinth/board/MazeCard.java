package Labyrinth.board;

import Labyrinth.treasure.Treasure;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 * Třída reprezentující hrací kámen
 * Třída reprezentuje hrací kámen s cestou a všechny akce s tím spojené.
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class MazeCard implements java.io.Serializable{
	/**
	 * Výčet směrů kudy se dá kámen opustit
	 */
	public static enum CANGO{
		LEFT, UP, RIGHT, DOWN; 
	}
	
	/**
	 * typ kamene.
	 * L - rovná cesta
	 * C - rohová cesta
	 * F - křižovatka
	 */
	public String type;
	
	private boolean left = false;
	private boolean up = false;
	private boolean right = false;
	private boolean down = false;
	/**
	 * aktuální stav otočení kamene
	 */
	public int state = 0;
	
	/**
	 * Záladní obrázek pro vykreslení.
	 * pokud má být na kámenu poklad, je zde uložen už spojený obrázek cesty a pokladu
	 */
	public ImageIcon icon;
	
	/**
	 * Obrázek pro vykreslení. Většinou stejný jko ImageIcon, ale zde se ukládá pokud na obrázku stojí hráč.
	 */
	public ImageIcon iconAct;
	
	/**
	 * Typ pokladu poku jej kámen má obsahovat
	 */
	public Treasure treasure;
	
	
	/*
	* C - rohova cest
	* F - Cesta ve tvaru T
	* L - Rovna cesta
	*/
	MazeCard(String s){
		this.type = s;
		this.left = true;
		this.iconAct = this.icon = new ImageIcon(getClass().getResource("/Images/stones/"+ this.type +".png"));
		if(s.equals("C") || s.equals("F")){
			this.up = true;
		}
		if(s.equals("L") || s.equals("F")){
			this.right = true;
		}
	}
	
	/**
	 * Vytvoří kartu o zadaném typu
	 * @param s typ karty
	 * @return vytvořenou kartu
	 */
	public static MazeCard create(String s){
		if(s.equals("C") == false && s.equals("L") == false && s.equals("F") == false){
			throw new IllegalArgumentException();
		}
		MazeCard ret = new MazeCard(s);
		return ret;
	}
	
	/**
	 * Testuje zda se dá kámen požadovaným směrem opustit
	 * @param dir směr kudy chceme jít
	 * @return boolean hodnotu zda jde projít
	 */
	public boolean canGo(CANGO dir){
		switch(dir){
			case LEFT:
				return this.left;
				
			case UP:
				return this.up;
				
			case RIGHT:
				return this.right;
				
			case DOWN:
				return this.down;
		}
		return false;
	}
	
	/**
	 * Otočí kámen doprava
	 */
	public void turnRight(){
		boolean temp = this.left;
		this.left = this.down;
		this.down = this.right;
		this.right = this.up;
		this.up = temp;
		this.state = (this.state + 1) % 3;
		rotateImage(0);
	}
	/**
	 * Otočí kámen doleva
	 */
	public void turnLeft(){
		boolean temp = this.left;
		this.left = this.up;
		this.up = this.right;
		this.right = this.down;
		this.down = temp;
		this.state = (this.state + 1) % 3;
		rotateImage(1);
	}
	
	/**
	 * otočí obrázek kamene
	 * @param state směr, kterým má být obrázek otočen (0/1)
	 */
	private void rotateImage(int state){
		int w = this.icon.getIconWidth();
		int h = this.icon.getIconHeight();
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB); 
		Graphics2D g2 = img.createGraphics();
		double x = (h-w)/2.0;
		double y = (w-h)/2.0;
		AffineTransform at = AffineTransform.getTranslateInstance(x,y);
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		if(state == 0){
			at.rotate(Math.toRadians(90), w/2.0, h/2.0);
		}
		else{
			at.rotate(Math.toRadians(-90), w/2.0, h/2.0);
		}
		g2.drawImage(this.icon.getImage(), at, null);
		g2.dispose();
		this.iconAct = this.icon = new ImageIcon(img);
	}
}
