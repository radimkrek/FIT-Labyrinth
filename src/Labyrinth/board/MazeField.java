package Labyrinth.board;
import Labyrinth.Main;
import Labyrinth.treasure.*;
import javax.swing.ImageIcon;
/**
 * Třída reprezentující políčko herní plochy.
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class MazeField implements java.io.Serializable{
	private int col;
	private int row;
	private MazeCard card = null;
	
	/**
	 * Konstruktor, který nastaví na kterých souřadnicích se pole nachází;
	 * @param row řádek (Y-ová souřadnice)
	 * @param col sloupec (X-ová souřadnice)
	 */
	MazeField(int row, int col){
		this.row = row;
		this.col = col;
	}
	/**
	 * Vrací aktuální typ pole
	 * @return typ pole
	 * @see Labyrinth.board.MazeCard
	 */
	public MazeCard getCard(){
		return this.card;
	}
	
	/**
	 * NAstaví typ pole
	 * @param c typ pole
	 * @see Labyrinth.board.MazeCard
	 */
	public void putCard(MazeCard c){
		this.card = c;
	}
	/**
	 * Vytvoří poklad na políčku.
	 * @param code číselné vyjadření, který poklad se má vygenerovat 
	 */
	public void createTreasure(int code){
		this.card.treasure = new Treasure(code, true);
		this.card.iconAct = this.card.icon = new ImageIcon(Main.connectImages(this.card.icon.getImage(), this.card.treasure.icon.getImage(), Main.CENTER));
	}
	/**
	 * @return vrátí sloupec
	 */
	public int col(){
		return this.col;
	}
	/**
	 * @return vrátí řádek
	 */
	public int row(){
		return this.row;
	}
}
