package Labyrinth.board;
import Labyrinth.Data;
import Labyrinth.Main;
import java.util.*;
import javax.swing.ImageIcon;
/**
 * Třída reprezentující herní plán.
 * Třída reprezentuje herní plán a všechny akce s tím spojené
 * @author Radim Křek
 * @author Patrik Unzeitig
 */
public class MazeBoard implements java.io.Serializable{
	Random r = new Random();
	/**
	 * Šířka pole
	 */
	private int size;
	/**
	 * počet zahnutých cest
	 */
	private int Ccount;
	/**
	 * počet rovných cest
	 */
	private int Lcount;
	/**
	 * počet křižovatek
	 */
	private int Fcount;
	/**
	 * Volný kámen pro posunování polem.
	 * @see Labyrinth.board.MazeCard
	 */
	public MazeCard free = null;
	private MazeCard oldFree = null;
	/**
	 * Pole reprezentující herní plochu
	 * @see Labyrinth.board.MazeField
	 */
	private MazeField[] board;

	private int[] usedTreasure = new int[Data.fieldSize*Data.fieldSize];
	private int arrayCounter = 0;
	
	private String lastMove = "0D";
	private String lastLastMove;
	
	private ImageIcon p1s = new ImageIcon(getClass().getResource("/Images/player/0_start.png"));
	private ImageIcon p2s = new ImageIcon(getClass().getResource("/Images/player/1_start.png"));
	private ImageIcon p3s = new ImageIcon(getClass().getResource("/Images/player/2_start.png"));
	private ImageIcon p4s = new ImageIcon(getClass().getResource("/Images/player/3_start.png"));
	
	/**
	 * Konstruktor nastaví velikost pole a vytvoří jej.
	 * @param n počet políček na jednom řádku
	 */
	MazeBoard(int n){
		this.size = n;
		this.board = new MazeField[this.size * this.size];
		for(int i = 0; i < this.size; i++){
			for(int j = 0; j < this.size; j++){
				MazeField field = new MazeField(i+1, j+1);
				this.board[i*this.size+j] = field;
			}
		}

		for(int i = 0; i < this.usedTreasure.length; i++){
			this.usedTreasure[i] = -1;
		}
	}
	
	private String getRandomCard(){
		String alphabet = "CLF";
		return Character.toString(alphabet.charAt(this.r.nextInt(alphabet.length())));
	}
	/**
	 * Vytvoří novou hru.
	 * Funkce vygeneruje políčka, které umístí do herního plánu, náhodně vygeneruje poklady a umístí hráče na start
	 */
	public void newGame(){
		int counter = 0;
		this.Ccount = this.Fcount = this.Lcount = (this.size*this.size) / 3;
		int diff = (this.size*this.size)-(this.Ccount*3);
		this.Fcount += diff;
		this.Fcount -= ((((this.size-1)/2)+1)*(((this.size-1)/2)+1))-4;
		this.Ccount -= 4;

		for(int i = 0; i < this.size; i++){
			for(int j = 0; j < this.size; j++){
				int k = i*this.size+j;
				MazeCard card;
				if((i == 0 && j == 0) || (i == 0 && j == this.size-1) || (i == this.size-1 && j == 0) || (i == this.size-1 && j == this.size-1)){
					card = MazeCard.create("C");
					if (i == 0 && j == 0){
						card.turnLeft();
						card.turnLeft();
						card.icon = card.iconAct = new ImageIcon(Main.connectImages(card.icon.getImage(), this.p1s.getImage(), Main.CENTER));
					}
					if (i == 0 && j == this.size-1){
						card.turnLeft();
						card.icon = card.iconAct = new ImageIcon(Main.connectImages(card.icon.getImage(), this.p2s.getImage(), Main.CENTER));
					}
					if (i == this.size-1 && j == 0){
						card.turnRight();
						card.icon = card.iconAct = new ImageIcon(Main.connectImages(card.icon.getImage(), this.p3s.getImage(), Main.CENTER));
					}
					if (i == this.size-1 && j == this.size-1){
						card.icon = card.iconAct = new ImageIcon(Main.connectImages(card.icon.getImage(), this.p4s.getImage(), Main.CENTER));
					}
				}
				else if(i % 2 == 0 && j % 2 == 0){
					card = MazeCard.create("F");
					if (i == 0){
						card.turnLeft();
						card.turnLeft();
					}
					if (j == 0){
						card.turnRight();
					}
					if (j == this.size-1){
						card.turnLeft();
					}
				}
				else{
					String type = "";
					for(;true;){
						type = getRandomCard();
						if(type.equals("C") && this.Ccount > 0){
							this.Ccount--;
							break;
						}
						else if(type.equals("L") && this.Lcount > 0){
							this.Lcount--;
							break;
						}
						else if(type.equals("F") && this.Fcount > 0){
							this.Fcount--;
							break;
						}
						else if(this.Ccount == 0 && this.Lcount == 0 && this.Fcount == 0){
							break;
						}
					}					
					card = MazeCard.create(type);
					int turn = this.r.nextInt(4);
					if(turn % 2 == 0){
						turn = this.r.nextInt(4);
						for(;turn > 0;turn--){
							card.turnRight();
						}
					}
					else{
						turn = this.r.nextInt(4);
						for(;turn > 0;turn--){
							card.turnLeft();
						}
					}
				}
				this.board[k].putCard(card);
			}
		}
		for(int z = 0; z < Data.treasuresCnt; z++){
					int pos = this.r.nextInt((Data.fieldSize*Data.fieldSize)-1);
					if(pos != 0 && pos!= Data.fieldSize-1
							&& pos != (Data.fieldSize*Data.fieldSize)-1
							&& pos != ((Data.fieldSize-1)*(Data.fieldSize))
							&& !Main.inArray(usedTreasure, pos)){
						this.usedTreasure[this.arrayCounter++] = pos;
						this.board[pos].createTreasure(z);
					}
					else{
						z--;
					}
				}
		this.free = MazeCard.create(getRandomCard());
	}
	/**
	 * Vrátí políčko z pozice X|Y
	 * @param r X-ová souřadnice
	 * @param c Y-ová souřadnice
	 * @return Políčko, které se nachází na daných souřadnicích
	 */
	public MazeField get(int r, int c){
		return this.board[((r)*this.size+(c))];
	}
	
	/**
	 * Vrátí políčko nacházející se na určité pozici v poli
	 * @param pos pozice v poli
	 * @return Políčko, které se nachází na dané pozici
	 */
	public MazeField get(int pos){
		return this.board[pos];
	}
	/**
	 * Funkce starající se o posun pole a kontrolu, aby nešlo navrátit poslední provedený tah
	 * @param mf políčko na které má být vložen volný kámen
	 * @return boolean hodnotu zda bylo posunuto
	 */
	public boolean shift(MazeField mf){
		this.oldFree = this.free;
		MazeCard temp;
		if(!this.lastMove.equals(mf.col()+"U") && mf.row() == 1 && mf.col() %2 == 0){
			temp = this.board[(this.size-1)*this.size+(mf.col()-1)].getCard();
			for(int i = this.size-1; i > 0; i--){
				this.board[i*this.size+mf.col()-1].putCard(this.board[(i-1)*this.size + mf.col()-1].getCard());
			}
			this.board[mf.col()-1].putCard(this.free);
			this.free = temp;
			this.lastLastMove = this.lastMove;
			this.lastMove = mf.col()+"D";
			return true;
		}
		else if(!this.lastMove.equals(mf.col()+"D") && mf.row() == this.size && mf.col() %2 == 0){
			temp = this.board[mf.col()-1].getCard();
			for(int i = 0; i < this.size-1; i++){
				this.board[i*this.size+mf.col()-1].putCard(this.board[(i+1)*this.size + mf.col()-1].getCard());
			}
			this.board[(mf.row()-1)*this.size+(mf.col()-1)].putCard(this.free);
			this.free = temp;
			this.lastLastMove = this.lastMove;
			this.lastMove = mf.col()+"U";
			return true;
		}
		else if(!this.lastMove.equals(mf.row()+"L") && mf.col() == 1 && mf.row() %2 == 0){
			temp = this.board[(mf.row()-1)*this.size+(this.size-1)].getCard();
			for(int i = this.size-1; i > 0; i--){
				this.board[(mf.row()-1)*this.size+i].putCard(this.board[(mf.row()-1)*this.size + i-1].getCard());
			}
			this.board[(mf.row()-1)*this.size].putCard(this.free);
			this.free = temp;
			this.lastLastMove = this.lastMove;
			this.lastMove = mf.row()+"R";
			return true;
		}
		else if(!this.lastMove.equals(mf.row()+"R") && mf.col() == this.size && mf.row() %2 == 0){
			temp = this.board[(mf.row()-1)*this.size].getCard();
			for(int i = 0; i < this.size-1; i++){
				this.board[(mf.row()-1)*this.size+i].putCard(this.board[(mf.row()-1)*this.size + i+1].getCard());
			}
			this.board[(mf.row()-1)*this.size+(this.size-1)].putCard(this.free);
			this.free = temp;
			this.lastLastMove = this.lastMove;
			this.lastMove = mf.row()+"L";
			return true;
		}
		return false;
	}
	
	/**
	 * Vytvoří hrací plán
	 * @param n šířka hracího plánu
	 * @return hrací plán
	 */
	public static MazeBoard createMazeBoard(int n){
		return new MazeBoard(n);
	}
	/**
	 * Vrátí aktuální volný kámen
	 * @return aktuální volný kámen
	 */
	public MazeCard getFreeCard(){
		return this.free;
	}
	/**
	 * vrátí pole do podoby před vložením kamene
	 */
	public void revertBoard(){
		int pos = Character.getNumericValue(this.lastMove.charAt(0));
		pos = pos-1;
		if(this.lastMove.charAt(1) == 'D'){
			this.lastMove = "NOTHING";
			pos = (Data.fieldSize*(Data.fieldSize-1))+pos;
			MazeField mf = this.board[pos];
			System.out.println(pos);
			shift(mf);
			this.lastMove = this.lastLastMove;
		}
		else if(this.lastMove.charAt(1) == 'U'){
			this.lastMove = "NOTHING";
			MazeField mf = this.board[pos];
			System.out.println(pos);
			shift(mf);
		}
		else if(this.lastMove.charAt(1) == 'L'){
			this.lastMove = "NOTHING";
			pos = Data.fieldSize*pos;
			MazeField mf = this.board[pos];
			System.out.println(pos);
			shift(mf);
			this.lastMove = this.lastLastMove;
		}
		else if(this.lastMove.charAt(1) == 'R'){
			this.lastMove = "NOTHING";
			System.out.println(pos);
			pos = Data.fieldSize*pos+Data.fieldSize-1;
			MazeField mf = this.board[pos];
			shift(mf);
			this.lastMove = this.lastLastMove;
		}
	}
	
}
